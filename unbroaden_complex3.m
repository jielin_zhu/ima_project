%unbroading process for samples based on complex baseline distribution

%import files to get complex baseline distribution and samples for
%different sizes of variance
load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;

x = -0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i))./length(x_center);
end;

Fig = figure;
plot(x,pdf_x);
axis([0 1 0 max(pdf_x)]);
title('PDF of Baseline Random Variable X','fontsize',20);
%print(Fig,'-depsc','final_x_distribution.eps')
%pause;


%samples based on distribution of x
load samples.dat
x_sample1 = samples; %directly get samples generated from the complex distribution of x
size(x_sample1)
pause;

%NUM = 100000;
%noise_var1 = min(x_variance);
%noise_var2 = max(x_variance);
noise_var1 = max(x_variance);
noise1 = sqrt(noise_var1)*randn(1,length(x_sample1));
load 'samples_y.dat';
y_sample1 = samples_y(:);
%{
y_sample1 = zeros(1,length(x_sample1));
for i=1:length(x_sample1);
    y_sample1(i) = x_sample1(i)+noise1(i);
end;
%}
%y_sample1 = x_sample1+noise1; %get sample y_i from fixed sample x and fixed variance of noise
%noise2 = sqrt(noise_var2)*randn(1,NUM);

%generate sample of x and y for noise with small variance
%x_sample_cum1 = rand(1,NUM);%assign values to pick Gaussian distribution
%x_sample1 = zeros(1,length(NUM));
%y_sample1 = zeros(1,NUM);
%for i=1:length(x_samp)
    %num_gaussian = floor(x_sample_cum1(i)/(1/length(x_center)))+1;
    %if num_gaussian ==51;
        %x_sample_cum1(i)
        %pause;
    %end;
    %x_sample1(i) = x_center(num_gaussian)+sqrt(x_variance(num_gaussian))*randn(1);
    
    %y_sample1(i) = x_sample1(i)+noise1(i);
%end;
Bin = 0:0.001:1;
[h_y1 y_bin1] = hist(y_sample1,Bin);
H_y1 = h_y1./length(y_sample1)./(y_bin1(2)-y_bin1(1));
%plot(y_bin1,H_y1,'r-.');
%pause;

%generate the cumulative distribution for sample y1
C_y = cumsum(h_y1./length(y_sample1));
variance_epsilon = noise_var1; %set the size of noise for unbroadening
y_bin = y_bin1; %set the bin for cumulative distribution
%pause;

%rewrite the characteristic function by matrix operation;
t1 = 0:0.01:380;%sqrt(2*log(25)/variance_epsilon);%sqrt(2*log(1/variance_epsilon^(1/4))/variance_epsilon);%
a = 320; %generalize the frequency range for a smooth correction on the boundary
t2 = t1(end)+0.01:0.01:t1(end)+a;
length_t2 = length(t2);
t = [t1';t2'];
dF_y1 = diff(C_y); %vector as 1*n
dF_y = dF_y1'; %require vector of the dF to be n*1
Y = y_bin(:,[1:length(y_bin)-1]);
EXP = exp(variance_epsilon.*t.^2./2);
ch_x_r = cos(t*Y)*dF_y;
ch_x_i = sin(t*Y)*dF_y;
Ch_x_r = EXP.*ch_x_r;
Ch_x_i = EXP.*ch_x_i;
%pause;


Fig2 = figure;
plot(t,Ch_x_r,'k-.','linewidth',1.5);
%hold on;plot(t,Ch_x_r0,'r-','linewidth',1.5);
hold on;plot(t,ch_x_r,'r-.','linewidth',1.5);hold on;


%add one extra smooth curve to make characteristic function end up at 0;
%t2 = t(end)+0.01:0.01:t(end)+1;
t_extra = t2';
y_convert = (-t2'+t1(end)+a)./a;
F_convert = zeros(1,length(y_convert));
for i=1:length(t2);
    F_convert(i) = (y_convert(i)^5/5-2*y_convert(i)^6/3+6*y_convert(i)^7/7-y_convert(i)^8/2+y_convert(i)^9/9)*630;
    Ch_x_r(length(t1)+i) = F_convert(i)*ch_x_r(length(t1)+i)*EXP(length(t1)+i);
    Ch_x_i(length(t1)+i) = F_convert(i)*ch_x_i(length(t1)+i)*EXP(length(t1)+i);
end;
plot(t_extra,Ch_x_r(length(t1)+1:end),'b-.','linewidth',1.5);
axis([0 700 -1 1]);
l = legend('Re(\phi_X)(unbroaden)','Re(\phi_Y)','Correction','location','southwest');
set(l,'FontSize',16);
%print(Fig2,'-depsc','final_characteristic.eps')


%figure;plot(y_convert,F_convert);
%t_full = [t; t_extra];
%Ch_x_r_full = [Ch_x_r; ch_x_r_extra];
%Ch_x_i_full = [Ch_x_i; ch_x_i_extra];

Ch_x_r0 = zeros(1,length(t));
Ch_x_i0 = zeros(1,length(t));
dx = x(2)-x(1);
for i=1:length(Ch_x_r0);
    j=1;
    while j<length(x);
        term_r_x0 = cos(t(i)*x(j))*pdf_x(j)*dx;
        Ch_x_r0(i) = Ch_x_r0(i)+term_r_x0;
        
        term_i_x0 = sin(t(i)*x(j))*pdf_x(j)*dx;
        Ch_x_i0(i) = Ch_x_i0(i)+term_i_x0;
        
        j=j+1;
    end;
end;


%hold on;plot(t_full,Ch_x_r_full,'r-');
%grid on;
%plot(t,Ch_x_r,'r-.');
%hold on;plot(t,Ch_x_r0,'r-','linewidth',1.5);
%hold on;plot(t,ch_x_r,'g:','linewidth',1.5);
%hold on;plot(t_full,Ch_x_r_full,'r-');
%grid on;


figure;
plot(t,Ch_x_i,'b-.','linewidth',1.5);
hold on;plot(t,Ch_x_i0,'r-','linewidth',1.5);
%hold on;plot(t,ch_x_i,'g:','linewidth',1.5);
%hold on;plot(t_full,Ch_x_i_full,'r-');
%grid on;
%pause;

