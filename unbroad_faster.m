%directly load characteristic function file
load('Char_x_approx.dat');
t_load = Char_x_approx(1,:); %1*n
ch_x_r_load = Char_x_approx(2,:);%1*n
ch_x_i_load = Char_x_approx(3,:);%1*n
%figure;plot(t,ch_x_i);

%transfer to column vector
t = t_load';
ch_x_r = ch_x_r_load';
ch_x_i = ch_x_i_load';

x1 = -0.1:0.01:1.1;
x = x1';

ch_x = complex(ch_x_r,ch_x_i);
PDF = fft(ch_x);
PDF_r = real(PDF);
x_cor = [1:1:length(t)]'*2*pi/t(end);
figure;plot(x_cor,PDF_r);
axis([0 2 0 3000]);

load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;

x = -0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i))./length(x_center);
end;
figure;
plot(x,pdf_x);