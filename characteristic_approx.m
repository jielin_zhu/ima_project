%try the unbroading process based on characteristic function

%generate data for x
x = randg(2,100000,1);%1 + 0.1.*randn(1000,1);
variance_epsilon = 0.1;
epsilon = sqrt(variance_epsilon).*randn(length(x),1);
y = x+epsilon;
Bin = -10:0.01:10;

[h_y, y_bin] = hist(y,Bin); %output the histograms and the bin index
H_y = h_y./length(y);
C_y = cumsum(H_y); %impirical probability for y
H_x = hist(x,Bin)./length(x);
C_x = cumsum(H_x); %impirical probability for x

%figure;plot(y_bin,H_y,'r-');hold on;plot(Bin,H_x,'b-.');
%figure;plot(Bin,C_y,'r-');hold on;plot(Bin,C_x,'b-.');

%generate the real and imaginary part for C_y/C_sigma in a certain range of
%t
t = -10:0.01:10;
Ch_x_r = zeros(1,length(t)); %represents the approximation of the real part of characteristic function for x
Ch_x_i = zeros(1,length(t)); %represents the approximation of the imaginary part of characteristic function for x
Ch_x_r0 = zeros(1,length(t));
Ch_x_i0 = zeros(1,length(t));
Ch_y_r = zeros(1,length(t));
Ch_y_i = zeros(1,length(t));
for i=1:length(Ch_x_r);
    j=1;
    while j<length(y_bin);
        term_r = cos(t(i)*y_bin(j))*(C_y(j+1)-C_y(j))*exp(variance_epsilon*t(i)^2/2);
        Ch_x_r(i) = Ch_x_r(i)+term_r;
        
        term_i = sin(t(i)*y_bin(j))*(C_y(j+1)-C_y(j))*exp(variance_epsilon*t(i)^2/2);
        Ch_x_i(i) = Ch_x_i(i)+term_i;
        
        term_r_y = cos(t(i)*y_bin(j))*(C_y(j+1)-C_y(j));
        Ch_y_r(i) = Ch_y_r(i)+term_r_y;
        
        term_i_y = sin(t(i)*y_bin(j))*(C_y(j+1)-C_y(j));
        Ch_y_i(i) = Ch_y_i(i)+term_i_y;
        
        j=j+1;
    end;
    
    %compute theoretical characteristic function for gamma distribution
    %with k = theta = 1;
    %Ch_x_r0(i) = 1/(1+t(i)^2);
    %Ch_x_i0(i) = t(i)/(1+t(i)^2);
    
    %compute theoretical characteristic function for gamma distribution
    %with k = 2, theta = 1;
    Ch_x_r0(i) = (1-t(i)^2)/(1+t(i)^2)^2;
    Ch_x_i0(i) = 2*t(i)/(1+t(i)^2)^2;
end;

figure(1);
plot(t,Ch_x_r,'k-.','linewidth',1.5);
hold on;plot(t,Ch_x_r0,'r-','linewidth',1.5);
hold on;plot(t,Ch_y_r,'g:','linewidth',1.5)
figure(2);
plot(t,Ch_x_i,'b-.','linewidth',1.5);
hold on;plot(t,Ch_x_i0,'r-','linewidth',1.5);
hold on;plot(t,Ch_y_i,'g:','linewidth',1.5)
pause;

%approximation of pdf of x based on inverse fourier transform of the
%approximated characteristic function
X = -2:0.1:10; %range of x for the approximated pdf
T = -5:0.001:5; %trancated range for inverse fourier transform
pdf_x = zeros(1,length(X)); %approximated pdf for x
for i=1:length(X);
    integrand = zeros(1,length(T));
    for j=1:length(integrand);
        k = 1;
        while k<length(y_bin);
        term = cos(T(j)*(X(i)-y_bin(k)))*(C_y(k+1)-C_y(k))*exp(variance_epsilon*T(j)^2/2);
        integrand(j) = integrand(j)+term;
        k = k+1;
        end;
    end;
    pdf_x(i) = trapz(T,integrand);
end;
pdf_theory = zeros(1,length(X));
for i=1:length(X);
    pdf_theory(i) = 1/gamma(2)*X(i)*exp(-X(i));
end;
figure;
plot(X,pdf_x./trapz(X,pdf_x),'k-','linewidth',1.5);hold on;plot(X,pdf_theory,'r-.','linewidth',1.5);hold on;plot(y_bin,H_y./(y_bin(2)-y_bin(1)),'b:');
axis([-3 10 0 0.4]);
