%generate complex distribution for x and generate samples of y based on it

%20 center and variance for each gaussian
x_center = rand(1,20);
var_scale = 10^(-5);
x_variance = var_scale.*rand(1,50);

x = -0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    %x_center(j)
    %x_variance(j)
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
    %figure(1);
    %plot(x,pdf);hold on;
    %pause;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i));
end;
%int = trapz(x,pdf_x);
Fig0 = figure;
plot(x,pdf_x./length(x_center));
%ylabel('Density','fontsize',30);
%xlabel('x','fontsize',30);
title('Baseline Distribution of x','fontsize',20)
print(Fig0,'-depsc','figure_complex_distribution.eps')

%samples based on distribution of x
NUM = 100000;
noise_var1 = min(x_variance);
noise_var2 = max(x_variance);
noise1 = sqrt(noise_var1)*randn(1,NUM);
noise2 = sqrt(noise_var2)*randn(1,NUM);

%generate sample of x and y for noise with small variance
x_sample_cum1 = rand(1,NUM);%assign values to pick Gaussian distribution
x_sample1 = zeros(1,length(NUM));
y_sample1 = zeros(1,length(NUM));
for i=1:length(x_sample_cum1)
    num_gaussian = floor(x_sample_cum1(i)/(1/length(x_center)))+1;
    if num_gaussian ==51;
        x_sample_cum1(i)
        pause;
    end;
    x_sample1(i) = x_center(num_gaussian)+sqrt(x_variance(num_gaussian))*randn(1);
    
    y_sample1(i) = x_sample1(i)+noise1(i);
end;
Bin = -5:0.001:5;
[h_y1 y_bin1] = hist(y_sample1,Bin);
H_y1 = h_y1./length(y_sample1)./(y_bin1(2)-y_bin1(1));
figure;
plot(y_bin1,H_y1,'r-');hold on;
plot(x,pdf_x./length(x_center),'k-.');
axis([-0.2 1.2 0 18])

%generate sample of x and y for noise with large variance
x_sample_cum2 = rand(1,NUM);%assign values to pick Gaussian distribution
x_sample2 = zeros(1,length(NUM));
y_sample2 = zeros(1,length(NUM));
for i=1:length(x_sample_cum2)
    num_gaussian = floor(x_sample_cum2(i)/(1/length(x_center)))+1;
    if num_gaussian ==51;
        x_sample_cum2(i)
        pause;
    end;
    x_sample2(i) = x_center(num_gaussian)+sqrt(x_variance(num_gaussian))*randn(1);
    
    y_sample2(i) = x_sample2(i)+noise2(i);
end;
%Bin = -5:0.001:5;
[h_y2 y_bin2] = hist(y_sample2,Bin);
H_y2 = h_y2./length(y_sample2)./(y_bin2(2)-y_bin2(1));
boxline_y1 = 0:0.01:15;
boxline_x1 = ones(1,length(boxline_y1)).*0.8;
boxline_x2 = 0.8:0.01:1;
boxline_y2 = ones(1,length(boxline_x2)).*15;
Fig1 = figure;
plot(y_bin2,H_y2,'b-');hold on;
plot(x,pdf_x./length(x_center),'k-.');hold on;
plot(boxline_x1,boxline_y1,'r:');hold on;
plot(boxline_x2,boxline_y2,'r:');
l = legend('PDF of y','PDF of x');
set(l,'FontSize',20);
axis([0 1 0 45])
print(Fig1,'-depsc','complex_simulation1.eps')
Fig2 = figure;
plot(y_bin2,H_y2,'b-');hold on;
plot(x,pdf_x./length(x_center),'k-.');
l = legend('PDF of y','PDF of x');
set(l,'FontSize',20);
axis([0.8 1 0 15])
print(Fig2,'-depsc','complex_simulation2.eps')

%generate cumulative probability function
C_x = cumsum(pdf_x./length(x_center).*(x(2)-x(1)));
C_y_sample1 = cumsum(H_y1).*(y_bin1(2)-y_bin1(1));
C_y_sample2 = cumsum(H_y2).*(y_bin2(2)-y_bin2(1));
Fig3 = figure;
plot(x,C_x,'k-.');hold on;plot(Bin,C_y_sample1,'r-');hold on;plot(Bin,C_y_sample2,'b-');
axis([0 1 0 1]);
l = legend('CDF of x','CDF of y(small noise)','CDF of y(large noise)','Location','SouthEast')
set(l,'FontSize',20);
print(Fig3,'-depsc','figure_cdf.eps')

Fig4 = figure;
plot(x,C_x,'k-.');hold on;plot(Bin,C_y_sample1,'r-');hold on;plot(Bin,C_y_sample2,'b-');
l=legend('CDF of x','CDF of y(small noise)','CDF of y(large noise)','Location','SouthEast');
set(l,'FontSize',20);
axis([0.2 0.28 0.2 0.3]);
print(Fig4,'-depsc','figure_cdf_zoomin.eps')