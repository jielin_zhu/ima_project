using PyPlot

# Plot of resistance vs. temperature for Mo/Cu proximity bilayer
function r_v_t()
    # NB: not actual data; taken by eye from figure 1 in Irwin and Hilton, 2005.
    t=[95.817,95.833,95.849,95.864,95.88,95.893,
       95.908,95.920,95.933,95.946,95.955,95.965,
       95.973,95.978,95.982,95.984,95.986,95.989,
       96.000,96.019,96.038,96.060,96.079,96.100,
       96.116,96.130,96.147,96.162,96.179]
    r=[0.5,0.5,0.5,0.5,0.5,0.5,
       1.4,2.3,4.5,6.7,8.8,11.6,
       14.9,18.1,22.5,27.4,32.3,38.3,
       43.6,47.1,48.8,50.5,51.4,52.9,
       53.9,54.8,55.7,56.2,56.8]
    plot(t,r,"o")
    ylim(-1,60)
    title("Mo-Cu bilayer superconducting transition (Irwin and Hilton, 2005)")
    xlabel("Temperature (mK)")
    ylabel("Resistance (m\$\\Omega\$)")
end
