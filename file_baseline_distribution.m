%generate series as pdf of the complex distribution of x, and output the
%file including fixed information

%20 center and variance for each gaussian
rng('default');
x_center = rand(1,20);
var_scale = 10^(-5);
x_variance = var_scale.*rand(1,50);

x = -0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    %x_center(j)
    %x_variance(j)
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
    %figure(1);
    %plot(x,pdf);hold on;
    %pause;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i))./length(x_center);
end;
figure;plot(x,pdf_x);
pause;

savefile = 'baseline_distribution.mat';
save(savefile, 'x', 'pdf_x')
savefile = 'baseline_distribution_parameter.mat';
save(savefile_parameter, 'x_center', 'x_variance')