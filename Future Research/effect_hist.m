%check whether the characteristic function depends on hist of matlab by
%comparing the l2 norm of the difference between the true distribution and
%sample distribution
%we can also generate the optimal a(f) based on the different
%characteristic function

%use the same sample set loading from samples.dat
load samples.dat
x_sample1 = samples;

%try a fixed random number generator for noise set to see the effect of
%only hist function
load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;
noise_var1 = min(x_variance);
noise_var2 = max(x_variance);
index_split = round(length(x_sample1)/10);
rng('default'); %fixed random number generator
noise = randn(length(x_sample1),1);
noise1 = sqrt(noise_var1).*noise(1:index_split,1); %noise group for small variance
noise2 = sqrt(noise_var2).*noise(index_split+1:end,1); %noise group for small variance
y1 = x_sample1(1:index_split)+noise1;
y2 = x_sample1(index_split+1:end)+noise2;

%load the true characteristic function of X
load char_real_test.dat
t = char_real_test(:,1);
ch_x_r = char_real_test(:,2);
load char_imag_test.dat
ch_x_i = char_imag_test(:,2);

%find the characteristic function based on dBin1 = 0.001;
%generate cumulative distribution function for y1 and y2
dBin1 = 0.0001;
Bin = 0:dBin1:1;
h_y1 = hist(y1,Bin);
H_y1 = h_y1./length(y1)./dBin1;
C_y1 = cumsum(h_y1./length(y1));
h_y2 = hist(y2,Bin);
H_y2 = h_y2./length(y2)./dBin1;
C_y2 = cumsum(h_y2./length(y2));
Fig3 = figure(3);plot(Bin,C_y1,'b-');hold on;plot(Bin,C_y2,'r-');hold on;
%compute characteristic function for y1 and y2
Y = Bin(:,[1:length(Bin)-1]);
dF_y1 = diff(C_y1)'; %vector as n*1
ch_y_r1 = cos(t*Y)*dF_y1;
ch_y_i1 = sin(t*Y)*dF_y1;
dF_y2 = diff(C_y2)'; %vector as n*1
ch_y_r2 = cos(t*Y)*dF_y2;
ch_y_i2 = sin(t*Y)*dF_y2;
%find modified char_y2 and find the optimal a(t)
ch_y_r2m = ch_y_r2.*exp((noise_var2-noise_var1).*t.^2./2);
ch_y_i2m = ch_y_i2.*exp((noise_var2-noise_var1).*t.^2./2);
v = ((ch_y_r2m-ch_y_r1).^2+(ch_y_i2m-ch_y_i1).^2).*2;
H = diag(v);
f = -2.*((ch_x_r-ch_y_r1).*(ch_y_r2m-ch_y_r1)+(ch_x_i-ch_y_i1).*(ch_y_i2m-ch_y_i1));
A11 = diag(ones(length(v)-1,1).*(-1));
A21 = diag(ones(length(v)-1,1));
A = [A11 zeros(length(v)-1,1)]+[zeros(length(v)-1,1) A21];
b = zeros(length(v)-1,1);
lb = zeros(length(v),1);
ub = ones(length(v),1);
opts = optimoptions('quadprog','Algorithm','interior-point-convex','TolFun',1e-10,'Display','off');
[x, fval] = quadprog(H,f,A,b,[],[],lb,ub,[],opts);
Fig2 = figure(2);plot(t,x,'k-');hold on;
%compute the l2 norm of the difference in the range of (0,t)
dis1 = complex(ch_x_r-ch_y_r1,ch_x_i-ch_y_i1);
dis2 = complex(ch_x_r-ch_y_r2,ch_x_i-ch_y_i2);
dis_c = complex(ch_x_r-x.*(ch_y_r2m - ch_y_r1)-ch_y_r1,ch_x_i-x.*(ch_y_i2m - ch_y_i1)-ch_y_i1);
L1 = zeros(length(t),1);
L2 = zeros(length(t),1);
Lc = zeros(length(t),1);
for i=1:length(t);
    L1(i) = norm(dis1(1:i));  
    L2(i) = norm(dis2(1:i));
    Lc(i) = norm(dis_c(1:i));
end;
Fig1 = figure(1);plot(t(1:5:end),L1(1:5:end),'b-');hold on;plot(t(1:5:end),L2(1:5:end),'r-');hold on;plot(t(1:5:end),Lc(1:5:end),'k-');hold on;

%find the characteristic function based on dBin1 = 0.0001;
%generate cumulative distribution function for y1 and y2
dBin2 = 0.00001;
Bin = 0:dBin2:1;
h_y1 = hist(y1,Bin);
H_y1 = h_y1./length(y1)./dBin2;
C_y1 = cumsum(h_y1./length(y1));
h_y2 = hist(y2,Bin);
H_y2 = h_y2./length(y2)./dBin2;
C_y2 = cumsum(h_y2./length(y2));
figure(3);plot(Bin,C_y1,'b-.');hold on;plot(Bin,C_y2,'r-.');hold off;
xlabel('Y','fontsize',40);
ylabel('CDF','fontsize',40);
l = legend('low variance large dBin','high variance large dBin','low variance small dBin','high variance small dBin','location','northwest');
set(l,'FontSize',15);
print(Fig3,'-depsc','figure_hist_CDF.eps');
%compute characteristic function for y1 and y2
Y = Bin(:,[1:length(Bin)-1]);
dF_y1 = diff(C_y1)'; %vector as n*1
ch_y_r1 = cos(t*Y)*dF_y1;
ch_y_i1 = sin(t*Y)*dF_y1;
dF_y2 = diff(C_y2)'; %vector as n*1
ch_y_r2 = cos(t*Y)*dF_y2;
ch_y_i2 = sin(t*Y)*dF_y2;
%find modified char_y2 and find the optimal a(t)
ch_y_r2m = ch_y_r2.*exp((noise_var2-noise_var1).*t.^2./2);
ch_y_i2m = ch_y_i2.*exp((noise_var2-noise_var1).*t.^2./2);
v = ((ch_y_r2m-ch_y_r1).^2+(ch_y_i2m-ch_y_i1).^2).*2;
H = diag(v);
f = -2.*((ch_x_r-ch_y_r1).*(ch_y_r2m-ch_y_r1)+(ch_x_i-ch_y_i1).*(ch_y_i2m-ch_y_i1));
A11 = diag(ones(length(v)-1,1).*(-1));
A21 = diag(ones(length(v)-1,1));
A = [A11 zeros(length(v)-1,1)]+[zeros(length(v)-1,1) A21];
b = zeros(length(v)-1,1);
lb = zeros(length(v),1);
ub = ones(length(v),1);
opts = optimoptions('quadprog','Algorithm','interior-point-convex','TolFun',1e-10,'Display','off');
[x, fval] = quadprog(H,f,A,b,[],[],lb,ub,[],opts);
figure(2);plot(t,x,'k:');hold off;
xlabel('frequency','fontsize',40);
ylabel('optimal \alpha','fontsize',40);
l = legend('large dBin','small dBin');
set(l,'FontSize',20);
print(Fig2,'-depsc','figure_hist_alpha.eps')
%compute the l2 norm of the difference in the range of (0,t)
dis1 = complex(ch_x_r-ch_y_r1,ch_x_i-ch_y_i1);
dis2 = complex(ch_x_r-ch_y_r2,ch_x_i-ch_y_i2);
dis_c = complex(ch_x_r-x.*(ch_y_r2m - ch_y_r1)-ch_y_r1,ch_x_i-x.*(ch_y_i2m - ch_y_i1)-ch_y_i1);
L1 = zeros(length(t),1);
L2 = zeros(length(t),1);
Lc = zeros(length(t),1);
for i=1:length(t);
    L1(i) = norm(dis1(1:i));  
    L2(i) = norm(dis2(1:i));
    Lc(i) = norm(dis_c(1:i));
end;
figure(1);plot(t(1:5:end),L1(1:5:end),'b:');hold on;plot(t(1:5:end),L2(1:5:end),'r:');hold on;plot(t(1:5:end),Lc(1:5:end),'k:');hold off;
axis([0 1000 0 2]);
xlabel('frequency','fontsize',40);
title({'L2 norm of the difference between';'estimated characteristic function and true one'},'fontsize',20);
print(Fig1,'-depsc','figure_hist_char.eps')