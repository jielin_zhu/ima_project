%choose dBin=10^(-5) to eliminate the effect of the hist
%use x_sample and the char_real/imag_test as the compare group to see if a
%different distribution of X can make large difference on alpha
%load information for new distribution, but keep noise size and sample size
%the same as the testing group

%load data from char_real/imag_test to find the l2 norm and optimal alpha
load char_real_test.dat
t = char_real(:,1);
ch_x_r = char_real(:,2);
ch_y_r1 = char_real(:,3); %real part for y with small variance
ch_y_r2 = char_real(:,4); %real part for y with large variance
load char_imag_test.dat
ch_x_i = char_imag(:,2);
ch_y_i1 = char_imag(:,3);
ch_y_i2 = char_imag(:,4);
load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;
noise_var1 = min(x_variance);
noise_var2 = max(x_variance);
ch_y_r2m = ch_y_r2.*exp((noise_var2-noise_var1).*t.^2./2);
ch_y_i2m = ch_y_i2.*exp((noise_var2-noise_var1).*t.^2./2);
%construct optimization problem in the form x'Hx/2+f'x
v = ((ch_y_r2m-ch_y_r1).^2+(ch_y_i2m-ch_y_i1).^2).*2;
H = diag(v);
f = -2.*((ch_x_r-ch_y_r1).*(ch_y_r2m-ch_y_r1)+(ch_x_i-ch_y_i1).*(ch_y_i2m-ch_y_i1));
A11 = diag(ones(length(v)-1,1).*(-1));
A21 = diag(ones(length(v)-1,1));
A = [A11 zeros(length(v)-1,1)]+[zeros(length(v)-1,1) A21];
b = zeros(length(v)-1,1);
lb = zeros(length(v),1);
ub = ones(length(v),1);
%use quadratic programming to find the optimal a(t)
opts = optimoptions('quadprog','Algorithm','interior-point-convex','TolFun',1e-10,'Display','off');
[x, fval] = quadprog(H,f,A,b,[],[],lb,ub,[],opts);
figure(1);%generate the optimal alpha for the testing group
plot(t,x,'r-');hold on;
%check the l2 norm of the difference between char of x and convex char of y
dis1 = complex(ch_x_r-ch_y_r1,ch_x_i-ch_y_i1);
dis2 = complex(ch_x_r-ch_y_r2,ch_x_i-ch_y_i2);
dis2m = complex(ch_x_r-ch_y_r2m,ch_x_i-ch_y_i2m);
dis_c = complex(ch_x_r-x.*(ch_y_r2m - ch_y_r1)-ch_y_r1,ch_x_i-x.*(ch_y_i2m - ch_y_i1)-ch_y_i1);
L1 = zeros(length(t),1);
L2 = zeros(length(t),1);
L2m = zeros(length(t),1);
Lc = zeros(length(t),1);
for i=1:length(t);
    L1(i) = norm(dis1(1:i));
    
    L2(i) = norm(dis2(1:i));
    L2m(i) = norm(dis2m(1:i));
    
    Lc(i) = norm(dis_c(1:i));
end;
figure(2);plot(t,L1,'b');hold on;plot(t,L2m,'g');hold on;plot(t,Lc,'k');hold on;plot(t,L2,'r');hold on;
axis([0 1000 0 2]);