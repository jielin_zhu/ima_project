%generate series as pdf of the complex distribution of x, and output the
%file including fixed information
%also generate 10

%20 center and variance for each gaussian
%rng('default'); %fixed generator of the random variable
num_peak = 20;
x_center = rand(num_peak,1);
var_scale = 10^(-5);
x_variance = var_scale.*rand(num_peak,1);

x = -0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    %x_center(j)
    %x_variance(j)
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
    %figure(1);
    %plot(x,pdf);hold on;
    %pause;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i))./length(x_center);
end;
figure;plot(x,pdf_x);
%pause;

%savefile = 'baseline_distribution.mat';
%save(savefile, 'x', 'pdf_x');
parameter = [x_center x_variance];
savefile_parameter = 'baseline_distribution_parameter.dat';
save(savefile_parameter, 'parameter','-ascii')

%generate sample of x
NUM = 100000;
x_sample_cum1 = rand(NUM,1);%assign values to pick Gaussian distribution
x_sample1 = zeros(NUM,1);
for i=1:length(x_sample_cum1)
    num_gaussian = floor(x_sample_cum1(i)/(1/length(x_center)))+1;
    if num_gaussian ==num_peak+1;
        x_sample_cum1(i)
        pause;
    end;
    x_sample1(i) = x_center(num_gaussian)+sqrt(x_variance(num_gaussian))*randn(1);
end;

savefile_sample = 'x_sample.dat';
save(savefile_sample,'x_sample1','-ascii');