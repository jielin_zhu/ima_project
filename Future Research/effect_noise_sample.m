%choose dBin=10^(-5) to eliminate the effect of the hist
%use the same sample of X but different noise samples to see whether
%alpha(f) dramatically changes
%same X but different Y1 and Y2

%use the same sample set loading from samples.dat
load samples.dat
x_sample1 = samples;

%load the baseline distribution parameter to set the variance of Y1 and Y2
load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;
noise_var1 = min(x_variance);
noise_var2 = max(x_variance);
index_split = round(length(x_sample1)/10);
%load the characteristic function of true distribution X
load char_real_test.dat
t = char_real_test(:,1);
ch_x_r = char_real_test(:,2);
load char_imag_test.dat
ch_x_i = char_imag_test(:,2);
%set the same Bin
dBin = 0.00001;
Bin = 0:dBin1:1;

%generate first random set of Y1 and Y2, compute the characteristic
%function, find optimal alpha(t), and compare the l2 norm
noise = randn(length(x_sample1),1);
noise1 = sqrt(noise_var1).*noise(1:index_split,1); %noise group for small variance
noise2 = sqrt(noise_var2).*noise(index_split+1:end,1); %noise group for small variance
y1 = x_sample1(1:index_split)+noise1;
y2 = x_sample1(index_split+1:end)+noise2;
h_y1 = hist(y1,Bin);
H_y1 = h_y1./length(y1)./dBin;
C_y1 = cumsum(h_y1./length(y1));
h_y2 = hist(y2,Bin);
H_y2 = h_y2./length(y2)./dBin;
C_y2 = cumsum(h_y2./length(y2));
%compute characteristic function for y1 and y2
Y = Bin(:,[1:length(Bin)-1]);
dF_y1 = diff(C_y1)'; %vector as n*1
ch_y_r1 = cos(t*Y)*dF_y1;
ch_y_i1 = sin(t*Y)*dF_y1;
dF_y2 = diff(C_y2)'; %vector as n*1
ch_y_r2 = cos(t*Y)*dF_y2;
ch_y_i2 = sin(t*Y)*dF_y2;
%find modified char_y2 and find the optimal a(t)
ch_y_r2m = ch_y_r2.*exp((noise_var2-noise_var1).*t.^2./2);
ch_y_i2m = ch_y_i2.*exp((noise_var2-noise_var1).*t.^2./2);
v = ((ch_y_r2m-ch_y_r1).^2+(ch_y_i2m-ch_y_i1).^2).*2;
H = diag(v);
f = -2.*((ch_x_r-ch_y_r1).*(ch_y_r2m-ch_y_r1)+(ch_x_i-ch_y_i1).*(ch_y_i2m-ch_y_i1));
A11 = diag(ones(length(v)-1,1).*(-1));
A21 = diag(ones(length(v)-1,1));
A = [A11 zeros(length(v)-1,1)]+[zeros(length(v)-1,1) A21];
b = zeros(length(v)-1,1);
lb = zeros(length(v),1);
ub = ones(length(v),1);
opts = optimoptions('quadprog','Algorithm','interior-point-convex','TolFun',1e-10,'Display','off');
[x, fval] = quadprog(H,f,A,b,[],[],lb,ub,[],opts);
Fig2 = figure(2);plot(t,x,'m-');hold on;
print(Fig2,'-depsc','figure_noise_alpha.eps')
%compute the l2 norm of the difference in the range of (0,t)
dis1 = complex(ch_x_r-ch_y_r1,ch_x_i-ch_y_i1);
dis2 = complex(ch_x_r-ch_y_r2,ch_x_i-ch_y_i2);
dis_c = complex(ch_x_r-x.*(ch_y_r2m - ch_y_r1)-ch_y_r1,ch_x_i-x.*(ch_y_i2m - ch_y_i1)-ch_y_i1);
L1 = zeros(length(t),1);
L2 = zeros(length(t),1);
Lc = zeros(length(t),1);
for i=1:length(t);
    L1(i) = norm(dis1(1:i));  
    L2(i) = norm(dis2(1:i));
    Lc(i) = norm(dis_c(1:i));
end;
Fig1 = figure(1);plot(t(1:5:end),L1(1:5:end),'b--');hold on;plot(t(1:5:end),L2(1:5:end),'r--');hold on;plot(t(1:5:end),Lc(1:5:end),'k--');hold on;
print(Fig1,'-depsc','figure_noise_char.eps');

%{
%generate the second random set of Y1 and Y2, compute the characteristic
%function, find optimal alpha(t) and compare the l2 norm
noise = randn(length(x_sample1),1);
noise1 = sqrt(noise_var1).*noise(1:index_split,1); %noise group for small variance
noise2 = sqrt(noise_var2).*noise(index_split+1:end,1); %noise group for small variance
y1 = x_sample1(1:index_split)+noise1;
y2 = x_sample1(index_split+1:end)+noise2;
h_y1 = hist(y1,Bin);
H_y1 = h_y1./length(y1)./dBin;
C_y1 = cumsum(h_y1./length(y1));
h_y2 = hist(y2,Bin);
H_y2 = h_y2./length(y2)./dBin;
C_y2 = cumsum(h_y2./length(y2));
%compute characteristic function for y1 and y2
Y = Bin(:,[1:length(Bin)-1]);
dF_y1 = diff(C_y1)'; %vector as n*1
ch_y_r1 = cos(t*Y)*dF_y1;
ch_y_i1 = sin(t*Y)*dF_y1;
dF_y2 = diff(C_y2)'; %vector as n*1
ch_y_r2 = cos(t*Y)*dF_y2;
ch_y_i2 = sin(t*Y)*dF_y2;
%find modified char_y2 and find the optimal a(t)
ch_y_r2m = ch_y_r2.*exp((noise_var2-noise_var1).*t.^2./2);
ch_y_i2m = ch_y_i2.*exp((noise_var2-noise_var1).*t.^2./2);
v = ((ch_y_r2m-ch_y_r1).^2+(ch_y_i2m-ch_y_i1).^2).*2;
H = diag(v);
f = -2.*((ch_x_r-ch_y_r1).*(ch_y_r2m-ch_y_r1)+(ch_x_i-ch_y_i1).*(ch_y_i2m-ch_y_i1));
A11 = diag(ones(length(v)-1,1).*(-1));
A21 = diag(ones(length(v)-1,1));
A = [A11 zeros(length(v)-1,1)]+[zeros(length(v)-1,1) A21];
b = zeros(length(v)-1,1);
lb = zeros(length(v),1);
ub = ones(length(v),1);
opts = optimoptions('quadprog','Algorithm','interior-point-convex','TolFun',1e-10,'Display','off');
[x, fval] = quadprog(H,f,A,b,[],[],lb,ub,[],opts);
figure(2);plot(t,x,'k:');hold off;
%compute the l2 norm of the difference in the range of (0,t)
dis1 = complex(ch_x_r-ch_y_r1,ch_x_i-ch_y_i1);
dis2 = complex(ch_x_r-ch_y_r2,ch_x_i-ch_y_i2);
dis_c = complex(ch_x_r-x.*(ch_y_r2m - ch_y_r1)-ch_y_r1,ch_x_i-x.*(ch_y_i2m - ch_y_i1)-ch_y_i1);
L1 = zeros(length(t),1);
L2 = zeros(length(t),1);
Lc = zeros(length(t),1);
for i=1:length(t);
    L1(i) = norm(dis1(1:i));  
    L2(i) = norm(dis2(1:i));
    Lc(i) = norm(dis_c(1:i));
end;
figure(1);plot(t(1:5:end),L1(1:5:end),'b:');hold on;plot(t(1:5:end),L2(1:5:end),'r:');hold on;plot(t(1:5:end),Lc(1:5:end),'k:');hold off;
%}