%directly load characteristic function file
%load('Char_x_approx.dat');
%t_load = Char_x_approx(1,:); %1*n
%ch_x_r_load = Char_x_approx(2,:);%1*n
%ch_x_i_load = Char_x_approx(3,:);%1*n
%figure;plot(t,ch_x_i);

%transfer to column vector
%t = t_full;%t_load';
%ch_x_r = ch_x_r_load';
%ch_x_i = ch_x_i_load';
%ch_x_r = Ch_x_r_full;
%ch_x_i = Ch_x_i_full;

%t_load';
%ch_x_r = ch_x_r_load';
%ch_x_i = ch_x_i_load';
ch_x_r = Ch_x_r;
ch_x_i = Ch_x_i;


%test for one value of x
%x1 = -0.1:0.01:1.1;
%x = 0.1;%x1';
%tic
%I_pos = ch_x_r.*cos(x*t)+ch_x_i.*sin(x*t);
%p = 2*trapz(T,I_pos)/2/pi;
%toc

%integration for a certain range of x
x1 = 0:0.001:1;%-0.1:0.001:1.1;
x = x1';
pdf_x_approx = zeros(length(x),1);
for i=1:length(x);
   I_pos = ch_x_r.*cos(x(i)*t)+ch_x_i.*sin(x(i)*t);
   pdf_x_approx(i) = 2*trapz(t,I_pos)/2/pi;
end;

pdf_x_filter = zeros(1,length(x));
for i=1:length(x);
    if pdf_x_approx(i) > 0;
        pdf_x_filter(i) = pdf_x_approx(i);
    end;
end;
int = trapz(x,pdf_x_filter);
Fig = figure;plot(x,pdf_x_filter./int,'k-');hold on;

load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;

x = 0:0.001:1;%-0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i))./length(x_center);
end;
plot(x,pdf_x,'r-.');hold on;
%pause;

load 'samples_y.dat';
y_sample = samples_y(:);
Bin = 0:0.001:1;%-0.1:0.001:1.1;
[h_y1 y_bin1] = hist(y_sample,Bin);
H_y1 = h_y1./length(y_sample)./(y_bin1(2)-y_bin1(1));
norm1 = sqrt(sum((pdf_x_filter./int-pdf_x).^2))
norm2 = sqrt(sum((H_y1-pdf_x).^2));
plot(y_bin1,H_y1,'b-');


%plot the zoom-in region for the final results
V_y = 0:0.01:6;
V_x1 = ones(1,length(V_y)).*0.52;
V_x2 = ones(1,length(V_y)).*0.6;
H_x = 0.52:0.01:0.6;
H_y = ones(1,length(H_x)).*6;
plot(V_x1,V_y,'g-','linewidth',1.5);hold on;
plot(V_x2,V_y,'g-','linewidth',1.5);hold on;
plot(H_x,H_y,'g-','linewidth',1.5);hold on;

axis([0 1 0 max(pdf_x)]);
l = legend('Approximation of D','PDF of x','PDF of y','location','northwest');
set(l,'FontSize',20);
%title(['Choose max(T) = ',num2str(t(end)),', with $$ L^2$$ norm = ',num2str(norm1)],'fontsize',15,'interpreter','latex')
print(Fig,'-depsc','final_unbroad_approx.eps')
pause;

Fig2 = figure;
plot(x,pdf_x_filter./int,'k-','linewidth',1.5);hold on;
plot(x,pdf_x,'r-.','linewidth',1.5);hold on;
plot(y_bin1,H_y1,'b-','linewidth',1.5);
axis([0.53 0.585 0 5.5]);
print(Fig2,'-depsc','final_unbroad_approx_zoomin.eps')


Fig3 = figure;
plot(x,pdf_x_filter./int,'k-');hold on;
plot(x,pdf_x,'r-.');hold on;
plot(y_bin1,H_y1,'b-');
l = legend('Approximation of D','PDF of x','PDF of y','location','northwest');
set(l,'FontSize',10);
pbaspect([1 1.5 1])
axis([0 1 0 max(pdf_x)]);
print(Fig3,'-depsc','final_unbroad_approx2.eps')

%for the orignal pdf without correction function in characteristic function
Fig4 = figure;
plot(x,pdf_x_filter./int,'k-');hold on;
plot(x,pdf_x,'r-.');hold on;
plot(y_bin1,H_y1,'b-');
%l = legend('Approximation of D','PDF of x','PDF of y','location','northwest');
%set(l,'FontSize',20);
%axis([0.53 0.585 0 5.5]);
axis([0 1 0 max(pdf_x)])
print(Fig4,'-depsc','final_unbroad_origin.eps')

%zoom in the origin and approx2 figures to see the change of bumps
Fig5 = figure;
plot(x,pdf_x_filter./int,'k-');hold on;
plot(x,pdf_x,'r-.');hold on;
plot(y_bin1,H_y1,'b-');
axis([0 1 0 1]);
pbaspect([3 1 1])
title(['With correction, $$L^2 $$ norm = ',num2str(norm1)],'fontsize',20,'interpreter','latex')
print(Fig5,'-depsc','final_unbroad_approx2_zoomin.eps')

Fig6 = figure;
plot(x,pdf_x_filter./int,'k-');hold on;
plot(x,pdf_x,'r-.');hold on;
plot(y_bin1,H_y1,'b-');
axis([0 1 0 1]);
pbaspect([3 1 1])
title(['Without correction, $$L^2 $$ norm = ',num2str(norm1)],'fontsize',20,'interpreter','latex')
print(Fig6,'-depsc','final_unbroad_origin_zoomin.eps')