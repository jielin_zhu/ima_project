%data generated from thin pdf and broaden by noise following large variance

%generate data for x
x = randg(1,10000,1);%1 + 0.1.*randn(1000,1);
variance_epsilon = 0.1;
epsilon = sqrt(variance_epsilon).*randn(length(x),1);
y = x+epsilon;
Bin = -10:0.01:10;

H_y = hist(y,Bin)./length(y);
C_y = cumsum(H_y);
H_x = hist(x,Bin)./length(x);
C_x = cumsum(H_x);
figure;plot(Bin,H_y,'r-');hold on;plot(Bin,H_x,'b-.');
figure;plot(Bin,C_y,'r-');hold on;plot(Bin,C_x,'b-.');

%generate data samples under two variance
%compare their empirical cdf and related characteristic function for
%different variance