%unbroading process for samples based on complex baseline distribution

%import files to get complex baseline distribution and samples for
%different sizes of variance
load means_stddevs.dat
x_center = means_stddevs(:,1);
x_std = means_stddevs(:,2);
x_variance = x_std.^2;

x = -0.1:0.001:1.1;
pdf = zeros(length(x_center),length(x));
for j=1:length(x_center);
for i=1:length(x);
    pdf(j,i) = exp(-(x(i)-x_center(j))^2/2/x_variance(j))/sqrt(2*pi*x_variance(j));
end;
end;

%sum up pdf(j,i) over length(x_center) to combine the effect of different
%Gaussian
pdf_x = zeros(1,length(x));
for i=1:length(x);
    pdf_x(i) = sum(pdf(:,i))./length(x_center);
end;
figure;
plot(x,pdf_x);hold on;
pause;

%samples based on distribution of x
load samples.dat
x_sample1 = samples; %directly get samples generated from the complex distribution of x

%NUM = 100000;
%noise_var1 = min(x_variance);
%noise_var2 = max(x_variance);
noise_var1 = max(x_variance);
noise1 = sqrt(noise_var1)*randn(1,length(x_sample1));
y_sample1 = zeros(1,length(x_sample1));
for i=1:length(x_sample1);
    y_sample1(i) = x_sample1(i)+noise1(i);
end;
%y_sample1 = x_sample1+noise1; %get sample y_i from fixed sample x and fixed variance of noise
%noise2 = sqrt(noise_var2)*randn(1,NUM);

%generate sample of x and y for noise with small variance
%x_sample_cum1 = rand(1,NUM);%assign values to pick Gaussian distribution
%x_sample1 = zeros(1,length(NUM));
%y_sample1 = zeros(1,NUM);
%for i=1:length(x_samp)
    %num_gaussian = floor(x_sample_cum1(i)/(1/length(x_center)))+1;
    %if num_gaussian ==51;
        %x_sample_cum1(i)
        %pause;
    %end;
    %x_sample1(i) = x_center(num_gaussian)+sqrt(x_variance(num_gaussian))*randn(1);
    
    %y_sample1(i) = x_sample1(i)+noise1(i);
%end;
Bin = -5:0.001:5;
[h_y1 y_bin1] = hist(y_sample1,Bin);
H_y1 = h_y1./length(y_sample1)./(y_bin1(2)-y_bin1(1));
plot(y_bin1,H_y1,'r-.');
pause;

%generate the cumulative distribution for sample y1
C_y = cumsum(h_y1./length(y_sample1));
variance_epsilon = noise_var1; %set the size of noise for unbroadening
y_bin = y_bin1; %set the bin for cumulative distribution

%generate characteristic function for H_y1
%generate the real and imaginary part for C_y/C_sigma in a certain range of
%t
t = 0:0.01:sqrt(2*log(20)/variance_epsilon);%-sqrt(2*log(10)/variance_epsilon):0.01:sqrt(2*log(10)/variance_epsilon);
%t = -1000:0.01:1000;
Ch_x_r = zeros(1,length(t)); %represents the approximation of the real part of characteristic function for x
Ch_x_i = zeros(1,length(t)); %represents the approximation of the imaginary part of characteristic function for x
Ch_y_r = zeros(1,length(t));
Ch_y_i = zeros(1,length(t));
for i=1:length(Ch_x_r);
    j=1;
    while j<length(y_bin);
        term_r = cos(t(i)*y_bin(j))*(C_y(j+1)-C_y(j));%*exp(variance_epsilon*t(i)^2/2);
        Ch_x_r(i) = Ch_x_r(i)+term_r;
        
        term_i = sin(t(i)*y_bin(j))*(C_y(j+1)-C_y(j));%*exp(variance_epsilon*t(i)^2/2);
        Ch_x_i(i) = Ch_x_i(i)+term_i;
        
        term_r_y = cos(t(i)*y_bin(j))*(C_y(j+1)-C_y(j));
        Ch_y_r(i) = Ch_y_r(i)+term_r_y;
        
        term_i_y = sin(t(i)*y_bin(j))*(C_y(j+1)-C_y(j));
        Ch_y_i(i) = Ch_y_i(i)+term_i_y;
        
        j=j+1;
    end;
    Ch_x_r(i) = Ch_x_r(i)*exp(variance_epsilon*t(i)^2/2);
    Ch_x_i(i) = Ch_x_i(i)*exp(variance_epsilon*t(i)^2/2);
end;
%compute the characteristic function for the actural x
Ch_x_r0 = zeros(1,length(t));
Ch_x_i0 = zeros(1,length(t));
dx = x(2)-x(1);
for i=1:length(Ch_x_r0);
    j=1;
    while j<length(x);
        term_r_x0 = cos(t(i)*x(j))*pdf_x(j)*dx;
        Ch_x_r0(i) = Ch_x_r0(i)+term_r_x0;
        
        term_i_x0 = sin(t(i)*x(j))*pdf_x(j)*dx;
        Ch_x_i0(i) = Ch_x_i0(i)+term_i_x0;
        
        j=j+1;
    end;
end;

figure;
plot(t,Ch_x_r,'k-.','linewidth',1.5);
hold on;plot(t,Ch_x_r0,'r-','linewidth',1.5);
hold on;plot(t,Ch_y_r,'g:','linewidth',1.5)
figure;
plot(t,Ch_x_i,'b-.','linewidth',1.5);
hold on;plot(t,Ch_x_i0,'r-','linewidth',1.5);
hold on;plot(t,Ch_y_i,'g:','linewidth',1.5)
pause;

%approximation of pdf of x based on inverse fourier transform of the
%approximated characteristic function
tic
X = 0.1;%-1:0.1:2; %range of x for the approximated pdf
T = 0:0.01:sqrt(2*log(20)/variance_epsilon);%-500:0.001:500; %trancated range for inverse fourier transform
pdf_x = zeros(1,length(X)); %approximated pdf for x
for i=1:length(X);
    integrand = zeros(1,length(T));
    for j=1:length(integrand);
        k = 1;
        while k<length(y_bin);
        term = cos(T(j)*(X(i)-y_bin(k)))*(C_y(k+1)-C_y(k))*exp(variance_epsilon*T(j)^2/2);
        integrand(j) = integrand(j)+term;
        k = k+1;
        end;
    end;
    %pdf_x(i) = trapz(T,integrand);
end;
toc